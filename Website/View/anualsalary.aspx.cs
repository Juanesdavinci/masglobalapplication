﻿using System;
using MasGlobalAppl.Models;
using MasGlobalAppl.Controllers;


public partial class View_anualsalary : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        // Clear feedback fields
        lblResult.Text = "";
        ClearData();

        // Call the controller intermediate 
        CtrlEmployee controller = new CtrlEmployee();
        string id = txtEmployeeID.Text;
        if (string.IsNullOrEmpty(id))
        {
            EntEmployee[] data = controller.GetEmployees();
            if (data == null)
            {
                setMessage(controller.Error, true);
                return;
            }
            FillData(data);
        }
        else
        {
            int n;
            if (!int.TryParse(id, out n))
            {
                setMessage("id not valid, please type a number", true);
                return;
            }
            EntEmployee data = controller.GetEmployee(n);
            if (data == null)
            {
                setMessage("No user were found with the id <b>" + id + "</b>", true);
                if (!string.IsNullOrEmpty(controller.Error))
                {
                    setMessage(controller.Error, true);
                }
                return;
            }
            FillData(data);
        }
    }

    #region helperMethods
    private void ClearData()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }

    private void FillData(EntEmployee[] data)
    {
        GridView1.DataSource = data;
        GridView1.DataBind();
    }

    private void FillData(EntEmployee data)
    {
        EntEmployee[] dataArray = { data };
        GridView1.DataSource = dataArray;
        GridView1.DataBind();
    }

    private void setMessage(string message, bool isError)
    {
        lblResult.ForeColor = (isError) ? System.Drawing.Color.Red : System.Drawing.Color.Black;
        lblResult.Text = message;
    }
    #endregion

}