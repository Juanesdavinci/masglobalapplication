﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="View_index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Juan Esteban Escudero | MASGLOBAL Application</title>
    <link href="../Resources/bootstrap-4.2.1-dist/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
        <div class=" container">
        <div class="m-sm-4">
    <form id="form1" runat="server">
    <div>
        <h2>MASGLOBAL Application</h2>
        <h3>Juan Esteban Escudero Gómez</h3>

        <p>Hello!

My name is Juan Esteban, I am a passionate developer from Envigado, I studied Game development at Sena and now I am finishing my Software Engineering degree at ITM. I like to play video games, sometimes write short stories. I am a computer geek, avid traveller and music lover.
</p><p>
I have worked with C# for the past five years developing video games with Unity 3D and web pages using ASP.Net core. I also worked with Php, html, javascript and Css. I worked as a team leader for the past year helping the company achieve its goals. I am looking for new opportunities to grow as a developer working on a bigger company with higher goals and better organization.  My current goal is to work on interesting and challenging projects.</p>
        <p>you can reach me out at <a href="https://co.linkedin.com/in/juanestebanescuderogomez">Linkedin</a> and <a href="https://twitter.com/juanesdavinci">Twitter</a>
        <br />
        <h3>Exercise</h3>
        <a href="anualsalary.aspx">Anual Salary</a>
    </div>
    </form>
            </div></div>
</body>
</html>
