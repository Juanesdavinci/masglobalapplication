﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="anualsalary.aspx.cs" Inherits="View_anualsalary" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Employee's Salary</title>
    <link href="../Resources/bootstrap-4.2.1-dist/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div class=" container">
        <div class="m-sm-4">
    <form id="form1" runat="server">
    <div>
        <h1>Employee salary</h1>
        <hr />
        <div class="form-group">
        <p>Please type the Employee's ID or leave it blank to get all employees' information</p>
         </div>
        <div class="form-group">
        <asp:TextBox ID="txtEmployeeID" runat="server" Width="208px" CssClass="form-control" BorderStyle="Groove" placeholder="ID"></asp:TextBox>
            </div>
        <div class="form-group">
        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" class="btn btn-info"/>
        </div>
        <asp:Label ID="lblResult" runat="server"></asp:Label>
        <br />
        <asp:GridView ID="GridView1" runat="server">
            <EditRowStyle BorderColor="#6600FF" />
        </asp:GridView>
        <br />

    </div>
    </form>
        </div>
    </div>
</body>
</html>
