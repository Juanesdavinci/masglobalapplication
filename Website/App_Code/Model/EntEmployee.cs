﻿namespace MasGlobalAppl.Models
{
    /// <summary>
    /// Eployee Model
    /// </summary>
    public class EntEmployee
    {
        #region properties
        public int id { get; set; }
        public string name { get; set; }
        public string contractTypeName { get; set; }
        public int roleId { get; set; }
        public string roleName { get; set; }
        public string roleDescription { get; set; }
        public double hourlySalary { get; set; }
        public double monthlySalary { get; set; }
        public double anualSalary { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// String override for testing
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "Empleado " + name
                + "<br> contractTypeName: " + contractTypeName
                + "<br> roleId: " + roleId
                + "<br> roleName: " + roleName
                + "<br> roleDescription: " + roleDescription
                + "<br> hourlySalary: " + hourlySalary
                + "<br> monthlySalary: " + monthlySalary;
        }
        /// <summary>
        /// Calculate anual salary base on ContactTypeName
        /// </summary>
        public void CalculateAnualSalary()
        {
            switch (contractTypeName)
            {
                case "HourlySalaryEmployee":
                    anualSalary = 120 * hourlySalary * 12;
                    break;
                case "MonthlySalaryEmployee":
                    anualSalary = monthlySalary * 12;
                    break;
                default:
                    anualSalary = monthlySalary * 12;
                    break;
            }
        }
        #endregion
    }
}