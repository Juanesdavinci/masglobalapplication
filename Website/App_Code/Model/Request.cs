﻿using System.IO;
using System.Net;

namespace MasGlobalAppl.Models
{
    /// <summary>
    /// Web request model
    /// </summary>
    public class Request
    {
        public string GetRequestString()
        {
            string url = @"http://masglobaltestapi.azurewebsites.net/api/Employees";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode != HttpStatusCode.OK)
                return null;

            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }
    }
}