﻿using MasGlobalAppl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace MasGlobalAppl.Controllers
{
    /// <summary>
    /// Employee Controller
    /// </summary>
    public class CtrlEmployee
    {
        #region properties
        public string Error { get; set; }
        #endregion

        #region Methods

        /// <summary>
        /// retrieves all the employees, this happens when no ID is provided
        /// </summary>
        /// <returns></returns>
        public EntEmployee[] GetEmployees()
        {
            Request request = new Request();
            try
            {
                string rawData = request.GetRequestString();
                if (rawData != null)
                {
                    EntEmployee[] result = JsonConvert.DeserializeObject<EntEmployee[]>(rawData);
                    Array.ForEach(result, e => e.CalculateAnualSalary());
                    request = null;
                    return result;
                }
                else
                {
                    Error = "An error occured retreaving eployees";
                    request = null;
                    return null;
                }
            }
            catch (Exception e)
            {
                Error = e.Message;
                request = null;
                return null;
            }

        }

        /// <summary>
        /// retrieves one eployee information, this happens when the Employee's ID is provided
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EntEmployee GetEmployee(int id)
        {
            Request request = new Request();
            try
            {
                string rawData = request.GetRequestString();
                if (rawData != null)
                {
                    EntEmployee[] result = JsonConvert.DeserializeObject<EntEmployee[]>(rawData);
                    EntEmployee selectedE = result.First(e => e.id == id);
                    selectedE.CalculateAnualSalary();
                    return selectedE;
                }
                else
                {
                    Error = "An error occured retreaving eployees";
                    request = null;
                    return null;
                }
            }
            catch (Exception e)
            {
                Error = e.Message;
                request = null;
                return null;
            }

        }
        #endregion
    }
}